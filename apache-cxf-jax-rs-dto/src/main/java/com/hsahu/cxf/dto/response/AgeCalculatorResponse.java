package com.hsahu.cxf.dto.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author hsahu
 *
 */
@XmlRootElement(name = "AgeCalculatorResponse")
public class AgeCalculatorResponse {

	@JsonProperty(value = "NAME")
	private String name;

	@JsonProperty(value = "AGE")
	private String age;

	public AgeCalculatorResponse(String name, String age) {
		this.age = age;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

}
