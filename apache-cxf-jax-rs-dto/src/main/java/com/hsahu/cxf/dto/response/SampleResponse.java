package com.hsahu.cxf.dto.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author hsahu
 *
 */
@XmlRootElement(name = "SampleResponse")
public class SampleResponse {

	@JsonProperty(value = "name")
	private String name;

	@JsonProperty(value = "age")
	private String age;

	public SampleResponse(String name, String age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

}
