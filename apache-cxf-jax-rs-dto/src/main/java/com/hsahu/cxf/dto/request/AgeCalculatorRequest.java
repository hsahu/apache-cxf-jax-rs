package com.hsahu.cxf.dto.request;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 
 * @author hsahu
 *
 */
@XmlRootElement(name = "AgeCalculatorRequest")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgeCalculatorRequest {

	private String name;

	private String age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

}
