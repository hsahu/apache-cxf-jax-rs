package com.hsahu.cxf.dto.response;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * This DTO will be send as json on any exception
 * 
 * @author himanshu.sahu
 *
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class ExceptionResponse {

	@JsonProperty
	String message;

	@JsonProperty
	Object exception;

	public ExceptionResponse() {
		super();
	}

	public ExceptionResponse(String message) {
		super();
		this.message = message;
	}

	public ExceptionResponse(String message, Object exception) {
		super();
		this.message = message;
		this.exception = exception;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setException(Object exception) {
		this.exception = exception;
	}
}
