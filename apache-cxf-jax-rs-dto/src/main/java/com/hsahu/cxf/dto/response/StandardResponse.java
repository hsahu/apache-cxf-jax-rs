package com.hsahu.cxf.dto.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

import com.hsahu.cxf.common.CommonConstant;

@XmlRootElement(name = "StandaredResponse")
public class StandardResponse<T> {

	@JsonProperty(value = "payload")
	private T payload;

	@JsonProperty(value = "message")
	private String message;

	public StandardResponse() {
		super();
		this.message = CommonConstant.OK;
	}

	public T getPayload() {
		return payload;
	}

	public void setPayload(T payload) {
		this.payload = payload;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
