# apache-cxf-jax-rs

## server end point


```shell
curl -X POST \
  http://127.0.0.1:8080/apache-cxf-jax-rs-core/services/age \
  -H 'Authorization: Basic dXNlcjpwYXNzd29yZA==' \
  -H 'Content-Type: application/json' \
  -d '{
	"name": "Himanshu",
	"age": 34
}'
```



```shell
curl -X GET \
  http://127.0.0.1:8080/apache-cxf-jax-rs-core/services/hello \
  -H 'Authorization: Basic dXNlcjpwYXNzd29yZA==' \
```



wadl URL

http://127.0.0.1:8080/apache-cxf-jax-rs-core/services?_wadl