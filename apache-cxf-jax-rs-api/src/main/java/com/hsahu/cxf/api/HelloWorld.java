package com.hsahu.cxf.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.hsahu.cxf.biz.exception.AccessDeniedException;
import com.hsahu.cxf.biz.exception.CommandException;
import com.hsahu.cxf.biz.exception.InternalServiceException;
import com.hsahu.cxf.dto.request.AgeCalculatorRequest;
import com.hsahu.cxf.dto.response.AgeCalculatorResponse;
import com.hsahu.cxf.dto.response.SampleResponse;
import com.hsahu.cxf.dto.response.StandardResponse;

@Path("/")
public interface HelloWorld {

	@GET
	@Path("/hello")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public StandardResponse<SampleResponse> sayHi();

	@POST
	@Path("/age")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public StandardResponse<AgeCalculatorResponse> calculateAge(AgeCalculatorRequest request);

	@GET
	@Path("/exception-test")
	public StandardResponse<SampleResponse> exceptionMapper()
			throws CommandException, InternalServiceException, AccessDeniedException, Exception;
}
