package com.hsahu.cxf.api.impl;

import java.util.Random;

import org.springframework.stereotype.Service;

import com.hsahu.cxf.api.HelloWorld;
import com.hsahu.cxf.biz.exception.AccessDeniedException;
import com.hsahu.cxf.biz.exception.CommandException;
import com.hsahu.cxf.biz.exception.InternalServiceException;
import com.hsahu.cxf.dto.request.AgeCalculatorRequest;
import com.hsahu.cxf.dto.response.AgeCalculatorResponse;
import com.hsahu.cxf.dto.response.SampleResponse;
import com.hsahu.cxf.dto.response.StandardResponse;

@Service("helloWorldImpl")
public class HelloWorldImpl implements HelloWorld {

	@Override
	public StandardResponse<SampleResponse> sayHi() {
		StandardResponse<SampleResponse> saResponse = new StandardResponse<SampleResponse>();
		SampleResponse sampleResponse = new SampleResponse("Himanshu Sahu", "23");
		saResponse.setPayload(sampleResponse);
		return saResponse;
	}

	@Override
	public StandardResponse<AgeCalculatorResponse> calculateAge(AgeCalculatorRequest request) {
		StandardResponse<AgeCalculatorResponse> agResponse = new StandardResponse<AgeCalculatorResponse>();
		AgeCalculatorResponse ageCalculatorResponse = new AgeCalculatorResponse(request.getName(), request.getAge());
		agResponse.setPayload(ageCalculatorResponse);
		return agResponse;
	}

	@Override
	public StandardResponse<SampleResponse> exceptionMapper()
			throws CommandException, InternalServiceException, AccessDeniedException, Exception {
		Random rand = new Random();
		int randomNumber = rand.nextInt();
		if (randomNumber % 5 == 0) {
			throw new InternalServiceException("number can not be multiple of 5", new SampleResponse("Himanshu", "23"));
		}
		if (randomNumber % 3 == 0) {
			throw new CommandException("Number can not be multiple of 3.");
		}
		if (randomNumber % 10 > 3) {
			throw new AccessDeniedException("number can not be multiple of 10");
		} else {
			throw new Exception();
		}
	}

}
