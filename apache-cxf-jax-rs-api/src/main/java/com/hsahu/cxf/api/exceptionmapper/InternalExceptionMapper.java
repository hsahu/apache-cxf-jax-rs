package com.hsahu.cxf.api.exceptionmapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import com.hsahu.cxf.biz.exception.AccessDeniedException;
import com.hsahu.cxf.biz.exception.CommandException;
import com.hsahu.cxf.biz.exception.InternalServiceException;
import com.hsahu.cxf.dto.response.ExceptionResponse;

/**
 * all the exception thrown by api will be catch here
 * 
 * @author himanshu.sahu
 *
 */
public class InternalExceptionMapper implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {
		if (exception instanceof InternalServiceException) {
			InternalServiceException ise = (InternalServiceException) exception;
			ExceptionResponse ex = new ExceptionResponse(ise.getMessage(), ise.getException());
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(ex).type(MediaType.APPLICATION_JSON).build();
		}
		if (exception instanceof AccessDeniedException || exception instanceof CommandException) {
			ExceptionResponse ex = new ExceptionResponse(exception.getMessage());
			return Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_JSON).entity(ex).build();
		}
		ExceptionResponse ex = new ExceptionResponse("unknown error");
		return Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_JSON).entity(ex).build();
	}

}
