package com.hsahu.cxf.filters;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

/**
 * Simple request loggers using request and respose filter
 * 
 * @author hsahu
 *
 */
public class LoggingFilter implements ContainerRequestFilter, ContainerResponseFilter {

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		System.out.println("[REQUEST HEADERS] : " + requestContext.getHeaders().toString());
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		System.out.println("[RESPONSE HEADERS] : " + responseContext.getHeaders().toString());
	}
}
