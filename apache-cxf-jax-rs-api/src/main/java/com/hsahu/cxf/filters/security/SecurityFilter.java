package com.hsahu.cxf.filters.security;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.StringTokenizer;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hsahu.cxf.dto.response.StandardResponse;

/**
 * Basic authorization filter
 * 
 * @author hsahu
 *
 */
public class SecurityFilter implements ContainerRequestFilter {

	private static final String AUTHORIZATION_HEADER_KEY = "Authorization";
	private static final String AUTHORIZATION_HEADER_PREFIX = "Basic";

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		String authToken = requestContext.getHeaders().getFirst(AUTHORIZATION_HEADER_KEY);

		if (authToken != null) {
			authToken = authToken.replaceFirst(AUTHORIZATION_HEADER_PREFIX, "").trim();
			String decodeString = new String(Base64.getDecoder().decode(authToken), Charset.forName("UTF-8"));
			StringTokenizer stringTokenizer = new StringTokenizer(decodeString, ":");
			String userName = stringTokenizer.nextToken();
			String password = stringTokenizer.nextToken();

			if ("user".equals(userName) && "password".equals(password)) {
				return;
			}
		}

		StandardResponse<String> unAuthorizedResponse = new StandardResponse<String>();

		unAuthorizedResponse.setMessage("UNAUTHORIZED");
		unAuthorizedResponse.setPayload("Unauthorized access/incorrect username or password");

		Response errorResponse = Response.status(Status.UNAUTHORIZED).header("content-type", "application/json")
				.entity(unAuthorizedResponse).build();

		requestContext.abortWith(errorResponse);
	}

}
