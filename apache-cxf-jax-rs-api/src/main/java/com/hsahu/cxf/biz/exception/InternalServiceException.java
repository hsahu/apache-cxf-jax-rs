package com.hsahu.cxf.biz.exception;

/**
 * InternalServiceException
 * 
 * @author himanshu.sahu
 *
 */
public class InternalServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Object exception;

	public InternalServiceException(String msg) {
		super(msg);
	}

	public InternalServiceException(String msg, Object exception) {
		super(msg);
		this.exception = exception;
	}

	public Object getException() {
		return exception;
	}
}
