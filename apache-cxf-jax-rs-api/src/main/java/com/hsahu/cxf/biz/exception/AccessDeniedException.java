package com.hsahu.cxf.biz.exception;

/**
 * AccessDeniedException
 * 
 * @author himanshu.sahu
 *
 */
public class AccessDeniedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccessDeniedException(String message) {
		super(message);
	}

}
