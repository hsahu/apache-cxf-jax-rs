package com.hsahu.cxf.biz.exception;

/**
 * CommandException
 * 
 * @author himanshu.sahu
 *
 */
public class CommandException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CommandException(String message) {
		super(message);
	}

}
